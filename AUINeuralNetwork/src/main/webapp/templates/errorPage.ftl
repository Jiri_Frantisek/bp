<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Error page</title>
    <link type="text/css" rel="stylesheet" href="../static/css/newStyles.css">
</head>
<body>
<div class="content">
    <h1>Error</h1>
    <p>Unexpected error occured. Try to restart the application.</p>
    <input type="button" onclick="location.href='/';" value="Back"/>
</div>
</body>

</html>