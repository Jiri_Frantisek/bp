<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link type="text/css" rel="stylesheet" href="../static/css/newStyles.css">
</head>
<body>
<div class="content">
    <h1>Input data</h1>
    <form id="input-nn-form">
        <label for="x">x: (around axis x in degrees) <input id="x" name="sensors[gyroscope][x]" type="number" min="0"
                                                            value="0"
                                                            max="360" step="1" required></label>
        <label for="y">y: (around axis y in degrees) <input id="y" name="sensors[gyroscope][y]" type="number" min="0"
                                                            value="0"
                                                            max="360" step="1" required></label>
        <label for="z">z: (around axis z in degrees) <input id="z" name="sensors[gyroscope][z]" type="number" min="0"
                                                            value="0"
                                                            max="360" step="1" required></label>
        <label for="lightning">Ambient brightness: (0-100) %<input id="lightning" name="sensors[light][lighting]" type="number"
                                                       value="0"
                                                       min="0" max="100" step="1"
                                                       required></label>
        <label for="distance">Distance: (distance in cm between device and object in front of it)<input id="distance"
                                                                                                        name="sensors[distance][distance]"
                                                                                                        type="number"
                                                                                                        min="0" max="20"
                                                                                                        step="1"
                                                                                                        value="0"
                                                                                                        required></label>
        <label for="anger">Anger: (true or false)<input class="emotions" id="anger" name="emotions[anger][intensity]" type="number"
                                                        value="0"
                                                        min="0" max="1" step="1" required></label>
        <label for="disgust">Disgust: (true or false)<input class="emotions" id="disgust" name="emotions[disgust][intensity]" value="0"
                                                            type="number" min="0" max="1" step="1"
                                                            required></label>
        <label for="happiness">Happiness: (true or false)<input class="emotions" id="happiness" name="emotions[happiness][intensity]"
                                                                value="0"
                                                                type="number" min="0" max="1" step="1"
                                                                required></label>
        <label for="sadness">Sadness: (true or false)<input class="emotions" id="sadness" name="emotions[sadness][intensity]" value="0"
                                                            type="number" min="0" max="1" step="1"
                                                            required></label>
        <input class="submit-button" type="submit" value="Submit">
    </form>
</div>

<script src="../static/js/script.js"></script>
</body>

</html>