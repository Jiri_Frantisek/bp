<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <style>
        p {
            font-size: 2em;
        }

        input {
            padding: 0.3em;
            font-size: 2em;
        }
    </style>
</head>
<body style="
        background-color: rgb(${backgroundR},${backgroundG},${backgroundB});
        color: rgb(${fontR},${fontG},${fontB});
        ">

<p>Rotate display ${turnToLeft} degrees left.</p>
<p>Set display intensity to ${displayIntensity}%.</p>
<p>Display ${displayOnOff}.</p>


<br>
<input type="button" onclick="location.href='/';" value="Back"/>
</body>

</html>