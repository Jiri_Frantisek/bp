
var emotionsInputs = $('input.emotions');
for (var i=0; i < emotionsInputs.length; i++) {
    var input = emotionsInputs[i];
    input.addEventListener('change', function (e) {
        var input = this;
        if ('1' == input.value) {
            setReadOnly(input);
        } else {
            unsetReadOnly();
        }
    });
}

$('#input-nn-form').on('submit', function (e) {
    e.preventDefault();
    var obj = $("#input-nn-form").serializeObject();
    $.ajax({
        type: 'POST',
        url: '/resolveData',
        data: JSON.stringify(obj),
        contentType: 'application/json',
        success: function () {
            window.location.href = "/getPage";
        },
        error: function (error) {
            window.location.href = "/errorPage";
        }
    });
    return false;
});

function setReadOnly(used) {
    for (var i=0; i < emotionsInputs.length; i++) {
        if(emotionsInputs[i].id !== used.id) {
            var input = emotionsInputs[i];
            input.setAttribute('readonly', true);
            input.style.backgroundColor = "#dddddd";
        }
    }
}

function unsetReadOnly() {
    for (var i=0; i < emotionsInputs.length; i++) {
        var input = emotionsInputs[i];
        input.removeAttribute('readonly');
        input.style.backgroundColor = "white";
    }
}


// This serializeObject function was taken from web page: https://guolvqi001.com/topics/33346/convert-form-data-to-javascript-object-with-jquery?order_by=vote_count&
(function ($) {
    $.fn.serializeObject = function () {

        var self = this,
            json = {},
            push_counters = {},
            patterns = {
                "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                "key": /[a-zA-Z0-9_]+|(?=\[\])/g,
                "push": /^$/,
                "fixed": /^\d+$/,
                "named": /^[a-zA-Z0-9_]+$/
            };


        this.build = function (base, key, value) {
            base[key] = value;
            return base;
        };

        this.push_counter = function (key) {
            if (push_counters[key] === undefined) {
                push_counters[key] = 0;
            }
            return push_counters[key]++;
        };

        $.each($(this).serializeArray(), function () {

            // skip invalid keys
            if (!patterns.validate.test(this.name)) {
                return;
            }

            var k,
                keys = this.name.match(patterns.key),
                merge = this.value,
                reverse_key = this.name;

            while ((k = keys.pop()) !== undefined) {

                // adjust reverse_key
                reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');

                // push
                if (k.match(patterns.push)) {
                    merge = self.build([], self.push_counter(reverse_key), merge);
                }

                // fixed
                else if (k.match(patterns.fixed)) {
                    merge = self.build([], k, merge);
                }

                // named
                else if (k.match(patterns.named)) {
                    merge = self.build({}, k, merge);
                }
            }

            json = $.extend(true, json, merge);
        });

        return json;
    };
})(jQuery);