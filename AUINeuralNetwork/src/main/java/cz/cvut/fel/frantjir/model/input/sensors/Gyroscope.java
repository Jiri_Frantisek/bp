package cz.cvut.fel.frantjir.model.input.sensors;

import cz.cvut.fel.frantjir.model.INode;
import cz.cvut.fel.frantjir.utils.Pair;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Gyroscope implements INode {

    private int x;

    private int y;

    private int z;

    @Override
    public void remap(List<Pair> list) {
        list.add(new Pair("x", x));
        list.add(new Pair("y", y));
        list.add(new Pair("z", z));
    }
}
