package cz.cvut.fel.frantjir.config;

import cz.cvut.fel.frantjir.neuralNetwork.NeuralNetworkFacade;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;

/**
 * Application configuration class, neural network initialization.
 */
@Configuration
public class NNConfig {

    private static final Logger LOG = LoggerFactory.getLogger(NNConfig.class);

    private final Environment env;


    @Autowired
    public NNConfig(Environment env) {
        this.env = env;
    }

    @Bean
    public NeuralNetworkFacade initNeuralNetworkFacade() {
        final String INPUT_EMOTIONS = env.getProperty("nn.learning.input.emotions");
        final String INPUT_SENSORS_GYRO = env.getProperty("nn.learning.input.sensors.gyroscope");
        final String INPUT_SENSORS_LIGHT = env.getProperty("nn.learning.input.sensors.lightning");
        final String INPUT_SENSORS_DIST = env.getProperty("nn.learning.input.sensors.distance");
        final String OUTPUT_EMOTIONS = env.getProperty("nn.learning.output.emotions");
        final String OUTPUT_SENSORS_GYRO = env.getProperty("nn.learning.output.sensors.gyroscope");
        final String OUTPUT_SENSORS_LIGHT = env.getProperty("nn.learning.output.sensors.lightning");
        final String OUTPUT_SENSORS_DIST = env.getProperty("nn.learning.output.sensors.distance");
        if (StringUtils.isBlank(INPUT_EMOTIONS) || StringUtils.isBlank(INPUT_SENSORS_GYRO) || StringUtils.isBlank(INPUT_SENSORS_LIGHT) || StringUtils.isBlank(INPUT_SENSORS_DIST)
                || StringUtils.isBlank(OUTPUT_EMOTIONS) || StringUtils.isBlank(OUTPUT_SENSORS_GYRO) || StringUtils.isBlank(OUTPUT_SENSORS_LIGHT) || StringUtils.isBlank(OUTPUT_SENSORS_DIST)) {
            LOG.error("Couldn't read path from property properties.");
            System.exit(1);
        }
        NeuralNetworkFacade nns = new NeuralNetworkFacade();
        nns.init(INPUT_EMOTIONS, INPUT_SENSORS_GYRO, INPUT_SENSORS_LIGHT, INPUT_SENSORS_DIST
                , OUTPUT_EMOTIONS, OUTPUT_SENSORS_GYRO, OUTPUT_SENSORS_LIGHT, OUTPUT_SENSORS_DIST);
        return nns;
    }
}
