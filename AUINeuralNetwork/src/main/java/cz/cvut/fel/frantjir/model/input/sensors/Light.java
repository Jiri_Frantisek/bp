package cz.cvut.fel.frantjir.model.input.sensors;

import cz.cvut.fel.frantjir.model.INode;
import cz.cvut.fel.frantjir.utils.Pair;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Light implements INode {

    private int lighting;

    @Override
    public void remap(List<Pair> list) {
        list.add(new Pair("lightning", lighting));
    }
}
