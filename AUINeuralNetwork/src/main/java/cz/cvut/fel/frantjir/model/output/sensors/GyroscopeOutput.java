package cz.cvut.fel.frantjir.model.output.sensors;

import cz.cvut.fel.frantjir.model.INode;
import cz.cvut.fel.frantjir.model.output.IOutputNode;
import cz.cvut.fel.frantjir.utils.Pair;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class GyroscopeOutput implements INode, IOutputNode {

    private int turnLeftCnt;

    @Override
    public void remap(List<Pair> list) {
        list.add(new Pair("turnLeftCnt", turnLeftCnt));
    }

    @Override
    public void mapNewOutput(List<Integer> list) {
        turnLeftCnt = list.get(0);
        list.remove(0);
        if (turnLeftCnt < 0) {
            turnLeftCnt = 0;
        }
        if (turnLeftCnt > 4) {
            turnLeftCnt = 4;
        }
    }
}
