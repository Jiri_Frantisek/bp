package cz.cvut.fel.frantjir.neuralNetwork;

/**
 * Class that collects all neural networks.
 */
public class NeuralNetworkFacade {

    private DistanceSensorNN distance;

    private LightSensorNN light;

    private GyroscopeSensorNN gyroscope;

    private EmotionsNN emotions;

    public NeuralNetworkFacade() {
    }

    /**
     * Neural networks initialization.
     * @param inputEmotionsPath emotions input file.
     * @param inputSensorsGyroPath gyroscope sensor input file.
     * @param inputSensorsLightPath light sensor input file.
     * @param inputSensorsDistPath distance sensor input file.
     * @param outputEmotionsPath emotions output file.
     * @param outputSensorsGyroPath gyroscope sensor output file.
     * @param outputSensorsLightPath light sensor output file.
     * @param outputSensorsDistPath distance sensor output file.
     */
    public void init(String inputEmotionsPath, String inputSensorsGyroPath, String inputSensorsLightPath, String inputSensorsDistPath,
                     String outputEmotionsPath, String outputSensorsGyroPath, String outputSensorsLightPath, String outputSensorsDistPath) {
        emotions = new EmotionsNN(inputEmotionsPath, outputEmotionsPath);
        gyroscope = new GyroscopeSensorNN(inputSensorsGyroPath, outputSensorsGyroPath);
        light = new LightSensorNN(inputSensorsLightPath, outputSensorsLightPath);
        distance = new DistanceSensorNN(inputSensorsDistPath, outputSensorsDistPath);
    }

    /**
     * sends input data to neural network
     * @param input input neural network data.
     * @return Neural network return.
     */
    public int[] computeSensorsGyro(double[] input) {
        return gyroscope.computeGyro(input);
    }

    /**
     * sends input data to neural network
     * @param input input neural network data.
     * @return Neural network return.
     */
    public int[] computeSensorsLight(double[] input) {
        return light.computeLight(input);
    }

    /**
     * sends input data to neural network
     * @param input input neural network data.
     * @return Neural network return.
     */
    public int[] computeSensorsDistance(double[] input) {
        return distance.computeDistance(input);
    }

    /**
     * sends input data to neural network
     * @param input input neural network data.
     * @return Neural network return.
     */
    public int[] computeEmotions(double[] input) {
        return emotions.computeEmotions(input);
    }
}
