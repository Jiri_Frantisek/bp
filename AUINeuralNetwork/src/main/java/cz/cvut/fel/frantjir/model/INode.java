package cz.cvut.fel.frantjir.model;

import cz.cvut.fel.frantjir.utils.Pair;

import java.util.List;

public interface INode {

    /**
     * method used for mapping data from objects to neural network input.
     * @param list list of Pairs in which will be new data from model data stored.
     */
    void remap(List<Pair> list);

}
