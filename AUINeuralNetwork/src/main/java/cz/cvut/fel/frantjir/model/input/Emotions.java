package cz.cvut.fel.frantjir.model.input;

import cz.cvut.fel.frantjir.model.INode;
import cz.cvut.fel.frantjir.model.input.emotions.Anger;
import cz.cvut.fel.frantjir.model.input.emotions.Disgust;
import cz.cvut.fel.frantjir.model.input.emotions.Happiness;
import cz.cvut.fel.frantjir.model.input.emotions.Sadness;
import cz.cvut.fel.frantjir.utils.Pair;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Emotions implements INode {

    private Anger anger;

    private Disgust disgust;

    private Happiness happiness;

    private Sadness sadness;

    @Override
    public void remap(List<Pair> list) {
        anger.remap(list);
        disgust.remap(list);
        happiness.remap(list);
        sadness.remap(list);
    }
}
