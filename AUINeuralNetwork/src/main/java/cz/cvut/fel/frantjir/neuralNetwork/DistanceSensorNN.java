package cz.cvut.fel.frantjir.neuralNetwork;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.frantjir.model.INode;
import cz.cvut.fel.frantjir.model.input.sensors.Distance;
import cz.cvut.fel.frantjir.model.output.sensors.DistanceOutput;
import cz.cvut.fel.frantjir.utils.ModelTransformUtils;
import org.apache.commons.lang3.StringUtils;
import org.encog.engine.network.activation.ActivationLinear;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.ml.train.MLTrain;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class storing Distance sensor neural network.
 */
public class DistanceSensorNN {

    private static final Logger LOG = LoggerFactory.getLogger(DistanceSensorNN.class);

    private BasicNetwork networkDistance;

    DistanceSensorNN(String inputFile, String outputFile) {
        this.initDistance(inputFile, outputFile);
    }

    private void initDistance(String inputFile, String outputFile) {
        double[][] input = new double[0][];
        double[][] output = new double[0][];
        try {
            input = getLearningDataDistFromFile(inputFile, true);
            output = getLearningDataDistFromFile(outputFile, false);
        } catch (Exception e) {
            LOG.error("Couldn't read output/input data from file.", e);
            System.exit(1);
        }
        if (input.length == 0 || output.length == 0 || output.length != input.length) {
            LOG.error("Input and output arrays size doesn't match");
            System.exit(1);
        }
        LOG.info("Initializing neural neuralNetwork for sensors distance.");
        networkDistance = new BasicNetwork();
        networkDistance.addLayer(new BasicLayer(new ActivationLinear(), true, 1));
        networkDistance.addLayer(new BasicLayer(new ActivationLinear(), true, 5));
        networkDistance.addLayer(new BasicLayer(new ActivationLinear(), true, 5));
        networkDistance.addLayer(new BasicLayer(new ActivationLinear(), true, 5));
        networkDistance.addLayer(new BasicLayer(new ActivationLinear(), true, 1));
        networkDistance.getStructure().finalizeStructure();
        networkDistance.reset();

        MLDataSet trainingSet = new BasicMLDataSet(input, output);

        MLTrain train = new ResilientPropagation(networkDistance, trainingSet);
        long iterations = 0;
        do {
            train.iteration();
            iterations++;
        } while (iterations < 2000);
        train.finishTraining();
        LOG.info("Done learning neural neuralNetwork for sensors distance. With " + iterations + " iterations and " + train.getError() + " % error.");
    }

    public int[] computeDistance(double[] inputData) {
        long startTime = System.nanoTime();
        double[] computed = networkDistance.compute(new BasicMLData(inputData)).getData();
        long estimatedTime = System.nanoTime() - startTime;
        LOG.info("Distance sensor NN finished computing in " + estimatedTime + " ns.");
        int[] data = new int[computed.length];
        for (int i = 0; i < data.length; i++) {
            data[i] = (int) Math.round(computed[i]);
        }
        return data;
    }

    private double[][] getLearningDataDistFromFile(String filePath, boolean input) throws IOException {
        File f = new File(filePath);
        if (!f.exists()) {
            LOG.warn("File \"" + filePath + "\" does not exists.");
            return new double[0][];
        }
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        ObjectMapper objectMapper = new ObjectMapper();

        String line;
        List<INode> roots = new ArrayList<>();
        while ((line = br.readLine()) != null) {
            if (StringUtils.isBlank(line))
                continue;
            if (input) {
                Distance data = objectMapper.readValue(line, Distance.class);
                roots.add(data);
            } else {
                DistanceOutput data = objectMapper.readValue(line, DistanceOutput.class);
                roots.add(data);
            }
        }
        return ModelTransformUtils.fromListOfObjectToNetworkInput(roots);
    }

}
