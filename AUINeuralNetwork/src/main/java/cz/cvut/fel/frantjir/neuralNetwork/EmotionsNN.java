package cz.cvut.fel.frantjir.neuralNetwork;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.frantjir.model.INode;
import cz.cvut.fel.frantjir.model.input.Emotions;
import cz.cvut.fel.frantjir.model.output.EmotionsOutput;
import cz.cvut.fel.frantjir.utils.ModelTransformUtils;
import org.apache.commons.lang3.StringUtils;
import org.encog.engine.network.activation.ActivationLinear;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.ml.train.MLTrain;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class storing Emotions neural network.
 */
public class EmotionsNN {

    private static final Logger LOG = LoggerFactory.getLogger(EmotionsNN.class);

    private BasicNetwork networkEmotions;

    EmotionsNN(String inputFile, String outputFile) {
        this.init(inputFile, outputFile);
    }

    private void init(String inputFile, String outputFile) {
        double[][] input = new double[0][];
        double[][] output = new double[0][];
        try {
            input = getLearningDataFromFile(inputFile, true);
            output = getLearningDataFromFile(outputFile, false);
        } catch (Exception e) {
            LOG.error("Couldn't read output/input data from file.", e);
            System.exit(1);
        }
        if (input.length == 0 || output.length == 0 || output.length != input.length) {
            LOG.error("Input and output arrays size doesn't match");
            System.exit(1);
        }
        LOG.info("Initializing neural neuralNetwork for emotions.");
        networkEmotions = new BasicNetwork();
        networkEmotions.addLayer(new BasicLayer(new ActivationLinear(), true, 4));
        networkEmotions.addLayer(new BasicLayer(new ActivationLinear(), true, 5));
        networkEmotions.addLayer(new BasicLayer(new ActivationLinear(), true, 6));
        networkEmotions.getStructure().finalizeStructure();
        networkEmotions.reset();

        MLDataSet trainingSet = new BasicMLDataSet(input, output);

        MLTrain train = new ResilientPropagation(networkEmotions, trainingSet);
        long iterations = 0;
        do {
            train.iteration();
            iterations++;
        } while (iterations < 2000);
        train.finishTraining();
        LOG.info("Done learning neural neuralNetwork for emotions. With " + iterations + " iterations and " + train.getError() + " % error.");
    }

    public int[] computeEmotions(double[] inputData) {
        long startTime = System.nanoTime();
        double[] computed = networkEmotions.compute(new BasicMLData(inputData)).getData();
        long estimatedTime = System.nanoTime() - startTime;
        LOG.info("Emotions NN finished computing in " + estimatedTime + " ns.");
        int[] data = new int[computed.length];
        for (int i = 0; i < data.length; i++) {
            data[i] = (int) Math.round(computed[i]);
        }
        return data;
    }

    private double[][] getLearningDataFromFile(String filePath, boolean input) throws IOException {
        File f = new File(filePath);
        if (!f.exists()) {
            LOG.warn("File \"" + filePath + "\" does not exists.");
            return new double[0][];
        }
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        ObjectMapper objectMapper = new ObjectMapper();

        String line;
        List<INode> roots = new ArrayList<>();
        while ((line = br.readLine()) != null) {
            if (StringUtils.isBlank(line))
                continue;
            if (input) {
                Emotions data = objectMapper.readValue(line, Emotions.class);
                roots.add(data);
            } else {
                EmotionsOutput data = objectMapper.readValue(line, EmotionsOutput.class);
                roots.add(data);
            }
        }
        return ModelTransformUtils.fromListOfObjectToNetworkInput(roots);
    }
}
