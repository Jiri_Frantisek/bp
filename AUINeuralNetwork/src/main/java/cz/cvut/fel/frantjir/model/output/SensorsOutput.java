package cz.cvut.fel.frantjir.model.output;

import cz.cvut.fel.frantjir.model.INode;
import cz.cvut.fel.frantjir.model.output.sensors.DistanceOutput;
import cz.cvut.fel.frantjir.model.output.sensors.GyroscopeOutput;
import cz.cvut.fel.frantjir.model.output.sensors.LightOutput;
import cz.cvut.fel.frantjir.utils.Pair;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class SensorsOutput implements INode, IOutputNode {

    private GyroscopeOutput gyroscopeOutput;

    private LightOutput lightOutput;

    private DistanceOutput distanceOutput;

    @Override
    public void remap(List<Pair> list) {
        gyroscopeOutput.remap(list);
        lightOutput.remap(list);
        distanceOutput.remap(list);
    }

    @Override
    public void mapNewOutput(List<Integer> list) {
        gyroscopeOutput = new GyroscopeOutput();
        lightOutput = new LightOutput();
        distanceOutput = new DistanceOutput();
        gyroscopeOutput.mapNewOutput(list);
        lightOutput.mapNewOutput(list);
        distanceOutput.mapNewOutput(list);
    }
}
