package cz.cvut.fel.frantjir.model;

import cz.cvut.fel.frantjir.model.input.Root;
import cz.cvut.fel.frantjir.model.output.OutputRoot;
import lombok.Getter;
import lombok.Setter;

/**
 * Class representing root of model data, both input and output.
 */
@Getter
@Setter
public class LearningInputData {

    private Root root;

    private OutputRoot outputRoot;
}