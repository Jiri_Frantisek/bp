package cz.cvut.fel.frantjir.model.output.emotions;

import cz.cvut.fel.frantjir.model.INode;
import cz.cvut.fel.frantjir.model.output.IOutputNode;
import cz.cvut.fel.frantjir.utils.Pair;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class FontColor implements INode, IOutputNode {

    private int R;

    private int G;

    private int B;

    @Override
    public void remap(List<Pair> list) {
        list.add(new Pair("fontColorR", R));
        list.add(new Pair("fontColorG", G));
        list.add(new Pair("fontColorB", B));
    }

    @Override
    public void mapNewOutput(List<Integer> list) {
        R = list.get(0);
        list.remove(0);
        G = list.get(0);
        list.remove(0);
        B = list.get(0);
        list.remove(0);
        if (R < 0) {
            R = 0;
        }
        if (G < 0) {
            G = 0;
        }
        if (B < 0) {
            B = 0;
        }
        if (R > 255) {
            R = 255;
        }
        if (G > 255) {
            G = 255;
        }
        if (B > 255) {
            B = 255;
        }
    }
}
