package cz.cvut.fel.frantjir.utils;

import cz.cvut.fel.frantjir.model.INode;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public final class ModelTransformUtils {

    private ModelTransformUtils() {
    }

    public static double[][] fromListOfObjectToNetworkInput(List<INode> list) {
        double[][] result = new double[list.size()][];
        List<Pair> pairs = new ArrayList<>();
        for (int i = 0; i < list.size(); i++) {
            list.get(i).remap(pairs);
            result[i] = fromListToArray(pairs);
            pairs.clear();
        }
        return result;
    }

    public static double[] fromListToArray(List<Pair> list) {
        List<Double> out = list.stream().map(Pair::getValue).map(Double::new).collect(Collectors.toList());
        double[] input = new double[list.size()];
        for (int i = 0; i < list.size(); i++) {
            input[i] = out.get(i);
        }
        return input;
    }

}
