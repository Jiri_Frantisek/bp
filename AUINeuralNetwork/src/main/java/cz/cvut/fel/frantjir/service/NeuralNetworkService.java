package cz.cvut.fel.frantjir.service;

import cz.cvut.fel.frantjir.model.input.Root;
import cz.cvut.fel.frantjir.model.output.EmotionsOutput;
import cz.cvut.fel.frantjir.model.output.OutputRoot;
import cz.cvut.fel.frantjir.model.output.SensorsOutput;
import cz.cvut.fel.frantjir.model.output.sensors.DistanceOutput;
import cz.cvut.fel.frantjir.model.output.sensors.GyroscopeOutput;
import cz.cvut.fel.frantjir.model.output.sensors.LightOutput;
import cz.cvut.fel.frantjir.neuralNetwork.NeuralNetworkFacade;
import cz.cvut.fel.frantjir.utils.ModelTransformUtils;
import cz.cvut.fel.frantjir.utils.Pair;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class NeuralNetworkService {

    private static final Logger LOG = LoggerFactory.getLogger(NeuralNetworkService.class);

    private final NeuralNetworkFacade neuralNetwork;

    private OutputRoot outputRoot = null;

    @Autowired
    public NeuralNetworkService(NeuralNetworkFacade neuralNetwork) {
        this.neuralNetwork = neuralNetwork;
    }

    public void resolve(Root root) {
        OutputRoot out = new OutputRoot();
        SensorsOutput sensOut = new SensorsOutput();
        sensOut.setGyroscopeOutput(new GyroscopeOutput());
        sensOut.setLightOutput(new LightOutput());
        sensOut.setDistanceOutput(new DistanceOutput());
        EmotionsOutput emoOut = new EmotionsOutput();
        List<Pair> sensorsGyro = new ArrayList<>();
        List<Pair> sensorsLight = new ArrayList<>();
        List<Pair> sensorsDist = new ArrayList<>();
        List<Pair> emotions = new ArrayList<>();
        root.getSensors().getGyroscope().remap(sensorsGyro);
        root.getSensors().getLight().remap(sensorsLight);
        root.getSensors().getDistance().remap(sensorsDist);
        root.getEmotions().remap(emotions);
        double[] inputSensGyro = ModelTransformUtils.fromListToArray(sensorsGyro);
        double[] inputSensLight = ModelTransformUtils.fromListToArray(sensorsLight);
        double[] inputSensDist = ModelTransformUtils.fromListToArray(sensorsDist);
        double[] inputEmo = ModelTransformUtils.fromListToArray(emotions);

        LOG.info("To NN sensors gyro: " + Arrays.toString(inputSensGyro));
        LOG.info("To NN sensors light: " + Arrays.toString(inputSensLight));
        LOG.info("To NN sensors dist: " + Arrays.toString(inputSensDist));
        LOG.info("To NN emotions: " + Arrays.toString(inputEmo));

        int[] computedSensGyro = neuralNetwork.computeSensorsGyro(inputSensGyro);
        int[] computedSensLight = neuralNetwork.computeSensorsLight(inputSensLight);
        int[] computedSensDist = neuralNetwork.computeSensorsDistance(inputSensDist);

        int[] computedEmo = neuralNetwork.computeEmotions(inputEmo);

        LOG.info("From NN sensors gyro: " + Arrays.toString(computedSensGyro));
        LOG.info("From NN sensors light: " + Arrays.toString(computedSensLight));
        LOG.info("From NN sensors dist: " + Arrays.toString(computedSensDist));
        LOG.info("From NN emotions: " + Arrays.toString(computedEmo));

        sensOut.getGyroscopeOutput().mapNewOutput(Arrays.stream(computedSensGyro).boxed().collect(Collectors.toList()));
        sensOut.getLightOutput().mapNewOutput(Arrays.stream(computedSensLight).boxed().collect(Collectors.toList()));
        sensOut.getDistanceOutput().mapNewOutput(Arrays.stream(computedSensDist).boxed().collect(Collectors.toList()));
        emoOut.mapNewOutput(Arrays.stream(computedEmo).boxed().collect(Collectors.toList()));

        out.setSensorsOutput(sensOut);
        out.setEmotionsOutput(emoOut);
        outputRoot = out;
    }

    public OutputRoot getLastOutputRoot() {
        return outputRoot;
    }
}
