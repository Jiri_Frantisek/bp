package cz.cvut.fel.frantjir.model.input;

import cz.cvut.fel.frantjir.model.INode;
import cz.cvut.fel.frantjir.utils.Pair;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Class representing neural network input data model root.
 */
@Getter
@Setter
public class Root implements INode {

    private Sensors sensors;

    private Emotions emotions;

    @Override
    public void remap(List<Pair> list) {
        sensors.remap(list);
        emotions.remap(list);
    }
}
