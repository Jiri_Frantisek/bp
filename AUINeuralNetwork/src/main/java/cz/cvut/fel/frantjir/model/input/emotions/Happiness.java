package cz.cvut.fel.frantjir.model.input.emotions;

import cz.cvut.fel.frantjir.model.INode;
import cz.cvut.fel.frantjir.utils.Pair;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Happiness implements INode {


    private int intensity;

    @Override
    public void remap(List<Pair> list) {
        list.add(new Pair("happinessIntensity", intensity));
    }
}
