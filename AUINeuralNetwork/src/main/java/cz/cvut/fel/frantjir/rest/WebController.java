package cz.cvut.fel.frantjir.rest;

import cz.cvut.fel.frantjir.model.input.Root;
import cz.cvut.fel.frantjir.model.output.OutputRoot;
import cz.cvut.fel.frantjir.service.NeuralNetworkService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

/**
 * Controller class, processing REST requests.
 */
@Controller
@RequestMapping("/")
public class WebController {

    private final NeuralNetworkService neuralNetworkService;

    @Autowired
    public WebController(NeuralNetworkService neuralNetworkService) {
        this.neuralNetworkService = neuralNetworkService;
    }

    @GetMapping("/")
    public String index(Model model) {
        return "index";
    }

    @GetMapping("/errorPage")
    public String getError(Model model) {
        return "errorPage";
    }

    @ResponseBody
    @RequestMapping(value = "/resolveData", method = RequestMethod.POST)
    public void resolve(@RequestBody Root data) {
        neuralNetworkService.resolve(data);
    }

    @GetMapping(value = "/getPage")
    public String getNewPage(Model model) {
        OutputRoot out = neuralNetworkService.getLastOutputRoot();
        model.addAttribute("backgroundR", out.getEmotionsOutput().getBackgroundColor().getR());
        model.addAttribute("backgroundG", out.getEmotionsOutput().getBackgroundColor().getG());
        model.addAttribute("backgroundB", out.getEmotionsOutput().getBackgroundColor().getB());
        model.addAttribute("fontR", out.getEmotionsOutput().getFontColor().getR());
        model.addAttribute("fontG", out.getEmotionsOutput().getFontColor().getG());
        model.addAttribute("fontB", out.getEmotionsOutput().getFontColor().getB());

        model.addAttribute("turnToLeft", (out.getSensorsOutput().getGyroscopeOutput().getTurnLeftCnt() * 90) % 360);
        model.addAttribute("displayIntensity", out.getSensorsOutput().getLightOutput().getDisplayIntensity());
        if (out.getSensorsOutput().getDistanceOutput().getDisplayOn() == 1) {
            model.addAttribute("displayOnOff", "ON");
        } else {
            model.addAttribute("displayOnOff", "OFF");
        }
        return "output";
    }


}
