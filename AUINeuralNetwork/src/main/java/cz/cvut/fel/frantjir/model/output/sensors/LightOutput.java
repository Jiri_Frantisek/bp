package cz.cvut.fel.frantjir.model.output.sensors;


import cz.cvut.fel.frantjir.model.INode;
import cz.cvut.fel.frantjir.model.output.IOutputNode;
import cz.cvut.fel.frantjir.utils.Pair;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class LightOutput implements INode, IOutputNode {

    private int displayIntensity;

    @Override
    public void remap(List<Pair> list) {
        list.add(new Pair("displayIntensity", displayIntensity));
    }

    @Override
    public void mapNewOutput(List<Integer> list) {
        displayIntensity = list.get(0);
        list.remove(0);
        if (displayIntensity < 0) {
            displayIntensity = 0;
        }
        if (displayIntensity > 100) {
            displayIntensity = 100;
        }
    }
}
