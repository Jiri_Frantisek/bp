package cz.cvut.fel.frantjir.model.output;

import java.util.List;

public interface IOutputNode {

    /**
     * method used for mapping neural network input to model data objects.
     * @param list list of neural network output.
     */
    void mapNewOutput(List<Integer> list);
}
