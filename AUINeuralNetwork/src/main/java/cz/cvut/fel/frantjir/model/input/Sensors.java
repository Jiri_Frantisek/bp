package cz.cvut.fel.frantjir.model.input;

import cz.cvut.fel.frantjir.model.INode;
import cz.cvut.fel.frantjir.model.input.sensors.Distance;
import cz.cvut.fel.frantjir.model.input.sensors.Gyroscope;
import cz.cvut.fel.frantjir.model.input.sensors.Light;
import cz.cvut.fel.frantjir.utils.Pair;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class Sensors implements INode {

    private Gyroscope gyroscope;

    private Light light;

    private Distance distance;

    @Override
    public void remap(List<Pair> list) {
        gyroscope.remap(list);
        light.remap(list);
        distance.remap(list);
    }
}
