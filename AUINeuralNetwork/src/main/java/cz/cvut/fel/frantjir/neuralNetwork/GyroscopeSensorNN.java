package cz.cvut.fel.frantjir.neuralNetwork;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.frantjir.model.INode;
import cz.cvut.fel.frantjir.model.input.sensors.Gyroscope;
import cz.cvut.fel.frantjir.model.output.sensors.GyroscopeOutput;
import cz.cvut.fel.frantjir.utils.ModelTransformUtils;
import org.apache.commons.lang3.StringUtils;
import org.encog.engine.network.activation.ActivationTANH;
import org.encog.ml.data.MLDataSet;
import org.encog.ml.data.basic.BasicMLData;
import org.encog.ml.data.basic.BasicMLDataSet;
import org.encog.ml.train.MLTrain;
import org.encog.neural.networks.BasicNetwork;
import org.encog.neural.networks.layers.BasicLayer;
import org.encog.neural.networks.training.propagation.resilient.ResilientPropagation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * Class storing Gyroscope sensor neural network.
 */
public class GyroscopeSensorNN {

    private static final Logger LOG = LoggerFactory.getLogger(GyroscopeSensorNN.class);

    private BasicNetwork networkGyro;

    GyroscopeSensorNN(String inputFile, String outputFile) {
        this.initGyro(inputFile, outputFile);
    }

    private void initGyro(String inputFile, String outputFile) {
        double[][] input = new double[0][];
        double[][] output = new double[0][];
        try {
            input = getLearningDataFromFile(inputFile, true);
            output = getLearningDataFromFile(outputFile, false);
        } catch (Exception e) {
            LOG.error("Couldn't read output/input data from file.", e);
            System.exit(1);
        }
        if (input.length == 0 || output.length == 0 || output.length != input.length) {
            LOG.error("Input and output arrays size doesn't match");
            System.exit(1);
        }
        LOG.info("Initializing neural neuralNetwork for sensors gyro.");
        networkGyro = new BasicNetwork();
        networkGyro.addLayer(new BasicLayer(new ActivationTANH(), true, 3));
        networkGyro.addLayer(new BasicLayer(new ActivationTANH(), true, 5));
        networkGyro.addLayer(new BasicLayer(new ActivationTANH(), true, 5));
        networkGyro.addLayer(new BasicLayer(new ActivationTANH(), true, 5));
        networkGyro.addLayer(new BasicLayer(new ActivationTANH(), true, 1));
        networkGyro.getStructure().finalizeStructure();
        networkGyro.reset();

        MLDataSet trainingSet = new BasicMLDataSet(input, output);
        MLTrain train = new ResilientPropagation(networkGyro, trainingSet);
        long iterations = 0;
        do {
            train.iteration();
            iterations++;
        } while (iterations < 2000);
        train.finishTraining();
        LOG.info("Done learning neural neuralNetwork for sensors gyro. With " + iterations + " iterations and " + train.getError() + " % error.");
    }

    public int[] computeGyro(double[] inputData) {
        long startTime = System.nanoTime();
        double[] computed = networkGyro.compute(new BasicMLData(inputData)).getData();
        long estimatedTime = System.nanoTime() - startTime;
        LOG.info("Gyroscope sensor NN finished computing in " + estimatedTime + " ns.");
        int[] data = new int[computed.length];
        for (int i = 0; i < data.length; i++) {
            data[i] = (int) Math.round(computed[i]);
        }
        return data;
    }

    private double[][] getLearningDataFromFile(String filePath, boolean input) throws IOException {
        File f = new File(filePath);
        if (!f.exists()) {
            LOG.warn("File \"" + filePath + "\" does not exists.");
            return new double[0][];
        }
        BufferedReader br = new BufferedReader(new FileReader(filePath));
        ObjectMapper objectMapper = new ObjectMapper();

        String line;
        List<INode> roots = new ArrayList<>();
        while ((line = br.readLine()) != null) {
            if (StringUtils.isBlank(line))
                continue;
            if (input) {
                Gyroscope data = objectMapper.readValue(line, Gyroscope.class);
                roots.add(data);
            } else {
                GyroscopeOutput data = objectMapper.readValue(line, GyroscopeOutput.class);
                roots.add(data);
            }
        }
        return ModelTransformUtils.fromListOfObjectToNetworkInput(roots);
    }
}
