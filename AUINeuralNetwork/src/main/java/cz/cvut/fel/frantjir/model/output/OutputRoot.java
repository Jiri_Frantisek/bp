package cz.cvut.fel.frantjir.model.output;

import cz.cvut.fel.frantjir.model.INode;
import cz.cvut.fel.frantjir.utils.Pair;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

/**
 * Class representing neural network output data model root.
 */
@Getter
@Setter
public class OutputRoot implements IOutputNode, INode {

    private SensorsOutput sensorsOutput;

    private EmotionsOutput emotionsOutput;

    @Override
    public void remap(List<Pair> list) {
        sensorsOutput.remap(list);
        emotionsOutput.remap(list);
    }

    @Override
    public void mapNewOutput(List<Integer> list) {
        sensorsOutput = new SensorsOutput();
        emotionsOutput = new EmotionsOutput();
        sensorsOutput.mapNewOutput(list);
        emotionsOutput.mapNewOutput(list);
    }
}
