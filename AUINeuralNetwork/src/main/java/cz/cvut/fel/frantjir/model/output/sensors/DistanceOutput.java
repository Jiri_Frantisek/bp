package cz.cvut.fel.frantjir.model.output.sensors;

import cz.cvut.fel.frantjir.model.INode;
import cz.cvut.fel.frantjir.model.output.IOutputNode;
import cz.cvut.fel.frantjir.utils.Pair;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class DistanceOutput implements INode, IOutputNode {

    private int displayOn;

    @Override
    public void remap(List<Pair> list) {
        list.add(new Pair("displayOn", displayOn));
    }

    @Override
    public void mapNewOutput(List<Integer> list) {
        displayOn = list.get(0);
        list.remove(0);
        if (displayOn < 0) {
            displayOn = 0;
        }
        if (displayOn > 1) {
            displayOn = 1;
        }
    }
}
