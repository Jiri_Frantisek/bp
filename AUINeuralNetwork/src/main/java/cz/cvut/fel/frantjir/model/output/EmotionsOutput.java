package cz.cvut.fel.frantjir.model.output;

import cz.cvut.fel.frantjir.model.INode;
import cz.cvut.fel.frantjir.model.output.emotions.BackgroundColor;
import cz.cvut.fel.frantjir.model.output.emotions.FontColor;
import cz.cvut.fel.frantjir.utils.Pair;
import lombok.Getter;
import lombok.Setter;

import java.util.List;


@Getter
@Setter
public class EmotionsOutput implements INode, IOutputNode {

    private BackgroundColor backgroundColor;

    private FontColor fontColor;

    @Override
    public void remap(List<Pair> list) {
        backgroundColor.remap(list);
        fontColor.remap(list);
    }

    @Override
    public void mapNewOutput(List<Integer> list) {
        backgroundColor = new BackgroundColor();
        fontColor = new FontColor();
        backgroundColor.mapNewOutput(list);
        fontColor.mapNewOutput(list);
    }
}
