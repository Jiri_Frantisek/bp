package cz.cvut.fel.frantjir.utils;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Pair {

    private String name;

    private int value;

    public Pair(String name, int value) {
        this.name = name;
        this.value = value;
    }
}
